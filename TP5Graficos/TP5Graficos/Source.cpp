#include <nana/gui.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana\gui\widgets\listbox.hpp>
#include <MugettiSimpleScoreLib\MugettiSimpleScoreLib.h>
#include <iostream>
#include <string>

int main()
{
	using namespace nana;

	//Define a form.
	form fm;

	nana::size sz;

	sz.height = 300;
	sz.width = 750;

	fm.size(sz);

	//nana::API::track_window_size(fm, { 800, 400 }, false);

	MugettiSimpleScoreLib score;

	score.RecordCurrentScore(150, "IGN");
	score.RecordCurrentScore(2000, "NAC");
	score.RecordCurrentScore(400, "ABX");
	score.RecordCurrentScore(4000, "IGN");
	score.RecordCurrentScore(5000, "MUG");

	std::string num = std::to_string(score.GetPositionScore(0));
	std::string num2 = std::to_string(score.GetPositionScore(1));
	std::string num3 = std::to_string(score.GetPositionScore(2));
	std::string num4 = std::to_string(score.GetPositionScore(3));
	std::string num5 = std::to_string(score.GetPositionScore(4));

	//Define a label and display a text.
	label lab{ fm, "<bold red size=16>Score:</>" };
	lab.format(true);

	//Define a button and answer the click event.
	button btn{ fm, "Quit" };
	btn.events().click([&fm] {
		fm.close();
	});

	listbox list{ fm, "Scores" };

	list.append_header("Position");
	list.append_header("Score");

	auto cat = list.at(0); //access the default category.

	cat.append({ "1", num });	//Insert an item
	cat.append({ "2", num2 });	//Insert an item
	cat.append({ "3", num3 });	//Insert an item
	cat.append({ "4", num4 });	//Insert an item
	cat.append({ "5", num5 });	//Insert an item

	list.auto_draw(true);

	
	//Layout management
	fm.div("vert <><<><text weight=10%><>><><weight=150<><listbox><>><>");
	fm["text"] << lab;
	fm["listbox"] << list;
	fm.collocate();

	//Show the form
	fm.show();

	//Start to event loop process, it blocks until the form is closed.
	exec();
}